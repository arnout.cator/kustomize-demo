#!/usr/bin/env bash
set -euo pipefail

# Set the image tag if not set
if [ -z "${IMAGE_TAG:-}" ]; then
    IMAGE_TAG=$(git rev-parse HEAD)
fi
export IMAGE_TAG

envsubst < k8s-base/kustomization.template.envsubst.yaml > k8s-base/kustomization.yaml
