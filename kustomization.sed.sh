#!/usr/bin/env bash
set -euo pipefail

# Set the image tag if not set
if [ -z "${IMAGE_TAG:-}" ]; then
    IMAGE_TAG=$(git rev-parse HEAD)
fi

sed "s/IMAGE_TAG/${IMAGE_TAG}/g" k8s-base/kustomization.template.sed.yaml > k8s-base/kustomization.yaml
