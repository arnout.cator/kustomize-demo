# Kustomize demo with dynamic variables

This repository contains an example of using Kustomize with configurations loaded from the environment variables.

Read more about it in the blog post ["Loading dynamic configurations in Kubernetes Kustomize"](https://medium.com/polarsquad/loading-dynamic-configurations-in-kubernetes-kustomize-95b80287db67).

## Files

* [`main.go`](main.go): a "Hello world" HTTP service written in Go
* [`Dockerfile`](Dockerfile): a Dockerfile for building a Docker image for the service
* [`k8s-base`](k8s-base/): a Kustomize project for deploying the service
* [`.gitlab-ci.yml`](.gitlab-ci.yml): a Gitlab CI pipeline for building the Docker image and publishing it
* [`kustomization.sed.sh`](kustomization.sed.sh): a script for templating `kustomization.yaml` file in the `k8s-base/` directory using `sed`
* [`kustomization.envsubst.sh`](kustomization.envsubst.sh): a script for templating `kustomization.yaml` file in the `k8s-base/` directory using `envsubst`
* [`kubernetes.yaml.template.sh`](kubernetes.yaml.template.sh): a script for generating a temporary Kustomize project based on the project in `k8s-base/` that contains configurations loaded from the environment variables

